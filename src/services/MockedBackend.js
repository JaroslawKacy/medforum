
/**
 * @type {string}
 */
const token = 'qwerty';

/**
 * @type {Array}
 */
const availableProducts = [
  {
    uuid:   'p1',
    name:   'Product 1',
    number: 5,
    photo:  'floppydisk',
    price:  '15 zł',
  },
  {
    uuid:   'p2',
    name:   'Product 2',
    number: 8,
    photo:  'iphone',
    price:  '10 zł',
  },
  {
    uuid:   'p3',
    name:   'Product 3',
    number: 7,
    photo:  'laptop',
    price:  '8 zł',
  },
  {
    uuid:   'p4',
    name:   'Product 4',
    number: 10,
    photo:  'pendrive',
    price:  '12 zł',
  },
  {
    uuid:   'p5',
    name:   'Product 5',
    number: 12,
    photo:  'pixel',
    price:  '25 zł',
  },
  {
    uuid:   'p6',
    name:   'Product 6',
    number: 3,
    photo:  'tablet',
    price:  '100 zł',
  },
];

/**
 * @type {Array}
 */
let basket = [];

/**
 * @type    {Function}
 * @returns {Array}
 */
const getBasket = function() {
  return basket;
};

/**
 * @type    {Function}
 * @returns {string}
 */
const getAvailableProducts = function() {
  return availableProducts;
};

/**
 * @type    {Function}
 * @param   {string}  productUuid
 * @returns {{Object | null}}
 */
const _getAvailableProduct = function(productUuid) {
  return getAvailableProducts().find(element => element.uuid === productUuid);
}

/**
 * @type    {Function}
 * @param   {string}  login
 * @param   {string}  password
 * @returns {string}
 */
const login = function(login, password) {

  return token;
};

/**
 * @type    {Function}
 * @param   {string}  productUuid
 * @returns {string}
 */
const addToBasket = function(productUuid) {
  const number      = 1;
  const inInBasket  = basket.some(element => element.uuid === productUuid);

  if (!inInBasket) {
    const product       = availableProducts.find(element => element.uuid === productUuid);
    const addedProduct  = {...product};
    addedProduct.number = number;

    basket.push(addedProduct);

    const added = _getAvailableProduct(productUuid);

    if (added) {
      added.number = added.number - number;
    }
  }

  return getBasket();
};

/**
 * @type    {Function}
 * @param   {string}  productUuid
 * @returns {string}
 */
const removeFromBasket = function(productUuid) {
  basket = basket.filter(element => {
    const availableProduct = _getAvailableProduct(productUuid);

    if (availableProduct && element.uuid === productUuid) {
      availableProduct.number = availableProduct.number + element.number;
    }

    return element.uuid !== productUuid;
  });

  return getBasket();
};

/**
 * @type    {Function}
 * @param   {string}  productUuid
 * @returns {string}
 */
const incrementProductInBasket = function(productUuid) {
  const inBasketProduct  = basket.find(element => element.uuid === productUuid);
  const availableProduct = _getAvailableProduct(productUuid);

  if (availableProduct && availableProduct.number > 0) {
    availableProduct.number = availableProduct.number - 1;
    inBasketProduct.number  = inBasketProduct.number + 1;
  }

  return {
    products:         getAvailableProducts(),
    productsInBasket: getBasket(),
  }
};

/**
 * @type    {Function}
 * @param   {string}  productUuid
 * @returns {string}
 */
const decrementProductInBasket = function(productUuid) {
  const inBasketProduct  = basket.find(element => element.uuid === productUuid);
  const availableProduct = _getAvailableProduct(productUuid);

  if (inBasketProduct && inBasketProduct.number > 1) {
    availableProduct.number = availableProduct.number + 1;
    inBasketProduct.number  = inBasketProduct.number - 1;
  }

  return {
    products:         getAvailableProducts(),
    productsInBasket: getBasket(),
  }
};

export {
  addToBasket,
  decrementProductInBasket,
  getAvailableProducts,
  getBasket,
  incrementProductInBasket,
  login,
  removeFromBasket
};
