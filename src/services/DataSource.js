import axios from 'axios';

// const baseUrl = 'http://medforumapi.develop';
const baseUrl = 'http://kacy.pl';

const axiosConfig = {
  headers: {
      'Accept':       'application/json',
      'Content-Type': 'application/json',
  }
};

/**
 * @type    {Function}
 * @param   {Object}  data
 * @returns {string}
 */
const prepareData = function(data) {
  return JSON.stringify(data);
}

/**
 * @type    {Function}
 * @param   {string}  endpoint
 * @param   {Object}  data
 * @returns {Promise}
 */
const post = function(endpoint = '', data = {}) {
  const url = baseUrl + endpoint;

  return axios.post(url, prepareData(data), axiosConfig);
}

export {
  post
};
